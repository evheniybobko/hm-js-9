function transformArrayList (arr, parent = "body") {
    let uList = document.createElement("ul");
    let parentElement = document.querySelector(`${parent}`);
    parentElement.prepend(uList);

    let listItem;

    arr.forEach(element => {
        listItem = document.createElement("li")
        listItem.innerText = element;
        uList.append(listItem);
    });

    return parentElement;
}

let arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let arr2 = ["1", "2", "3", "sea", "user", 23];

let result = transformArrayList(arr1, "header");
console.log(result);



/* 1.Опишіть, як можна створити новий HTML тег на сторінці.

 За допомогою методу - document.createElement(tag)
 Наприклад створення тегу div :
 let div = document.createElement('div');
 */


 /* 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

 - targetElement.insertAdjacentHTML(position, text):
 - Перший параметр позиція - визначає позицію елемента, що додається щодо елемента, що викликав метод. Повинно відповідати одному з наступних значень:
 'beforebegin': до відкриваючого тега.
 'afterbegin': перед першим нащадком.
 'beforeend': після останнього нащадка.
 'afterend': після закриваючого тега.
 - Другий параметр текст - рядок, який буде проаналізовано як HTML або XML і вставлено в DOM дерево документа.
 */


 /* 3.Як можна видалити елемент зі сторінки?
 
 За допомогою методу remove()
 Наприклад:
 div.remove();
 */